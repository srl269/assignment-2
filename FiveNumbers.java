//Scott Lafoon 2/5/14 

import java.util.Scanner;

public class FiveNumbers
{
public static void main(String[] args)
{

double n1, n2, n3, n4, n5, sum, average, med, min, max, mode, second;

Scanner keyboard = new Scanner(System.in);

//prompt the user to enter five numbers and set the values

System.out.println("Enter five numbers:");

System.out.println("Enter the first number:");
n1 = keyboard.nextDouble();

System.out.println("Enter the second number:");
n2 = keyboard.nextDouble();

System.out.println("Enter the third number:");
n3 = keyboard.nextDouble();

System.out.println("Enter the fourth number:");
n4 = keyboard.nextDouble();

System.out.println("Enter the fifth number:");
n5 = keyboard.nextDouble();
//find the sum by putting 5 values into the sum eqaution
sum = n1 + n2 + n3 + n4 + n5;
System.out.println("Sum:" +sum);

//find the average by putting 5 values into the average eqaution

average = (n1 + n2 + n3 + n4 + n5)/5 ;
System.out.println("Average:" +average);

//find min by setting one value as the min and if any of the other 4 are smaller they become the min
min = n1;
if (n2< min)
	min = n2;
if (n3< min)
	min = n3;
if (n4<min)
	min =n4;
if (n5<min)
	min =n5;
System.out.println("Min: "+min);

//find max by setting one value as the max and if any of the other 4 are larger they become the max
max = n1;
if (n2> max)
	max = n2;
if (n3> max)
	max = n3;
if (n4>max)
	max =n4;
if (n5>max)
	max =n5;
System.out.println("Max: "+max);

//figure out all the possible arrangements of the five values and the one that it is will set the median
med = n1;
if (n1>=n5&&n2>=n5&&n5>=n3&&n5>=n4)
	med = n5;
if (n1>=n5&&n2>=n5&&n5>=n4&&n5>=n3)
	med = n5;
if (n1>=n5&&n4>=n5&&n5>=n3&&n5>=n2)
	med = n5;
if (n1>=n5&&n4>=n5&&n5>=n2&&n5>=n3)
	med = n5;
if (n1>=n5&&n3>=n5&&n5>=n2&&n5>=n4)
	med = n5;
if (n1>=n5&&n3>=n5&&n5>=n2&&n5>=n4)
	med = n5;

if (n2>=n5&&n1>=n5&&n5>=n3&&n5>=n4)
	med = n5;
if (n2>=n5&&n1>=n5&&n5>=n4&&n5>=n3)
	med = n5;
if (n2>=n5&&n4>=n5&&n5>=n3&&n5>=n1)
	med = n5;
if (n2>=n5&&n4>=n5&&n5>=n1&&n5>=n3)
	med = n5;
if (n2>=n5&&n3>=n5&&n5>=n1&&n5>=n4)
	med = n5;
if (n2>=n5&&n3>=n5&&n5>=n1&&n5>=n4)
	med = n5;
	
if (n3>=n5&&n2>=n5&&n5>=n1&&n5>=n4)
	med = n5;
if (n3>=n5&&n2>=n5&&n5>=n4&&n5>=n1)
	med = n5;
if (n3>=n5&&n4>=n5&&n5>=n1&&n5>=n2)
	med = n5;
if (n3>=n5&&n4>=n5&&n5>=n2&&n5>=n1)
	med = n5;
if (n3>=n5&&n1>=n5&&n5>=n2&&n5>=n4)
	med = n5;
if (n3>=n5&&n1>=n5&&n5>=n2&&n5>=n4)
	med = n5;

if (n4>=n5&&n2>=n5&&n5>=n3&&n5>=n1)
	med = n5;
if (n4>=n5&&n2>=n5&&n5>=n1&&n5>=n3)
	med = n5;
if (n4>=n5&&n1>=n5&&n5>=n3&&n5>=n2)
	med = n5;
if (n4>=n5&&n1>=n5&&n5>=n2&&n5>=n3)
	med = n5;
if (n4>=n5&&n3>=n5&&n5>=n2&&n5>=n1)
	med = n5;
if (n4>=n5&&n3>=n5&&n5>=n2&&n5>=n1)
	med = n5;

if (n1>=n4&&n2>=n4&&n4>=n3&&n4>=n5)
	med = n4;
if (n1>=n4&&n2>=n4&&n4>=n5&&n4>=n3)
	med = n4;
if (n1>=n4&&n5>=n4&&n4>=n3&&n4>=n2)
	med = n4;
if (n1>=n4&&n5>=n4&&n4>=n2&&n4>=n3)
	med = n4;
if (n1>=n4&&n3>=n4&&n4>=n2&&n4>=n5)
	med = n4;
if (n1>=n4&&n3>=n4&&n4>=n2&&n4>=n5)
	med = n4;

if (n2>=n4&&n1>=n4&&n4>=n3&&n4>=n5)
	med = n4;
if (n2>=n4&&n1>=n4&&n4>=n5&&n4>=n3)
	med = n4;
if (n2>=n4&&n5>=n4&&n4>=n3&&n4>=n1)
	med = n4;
if (n2>=n4&&n5>=n4&&n4>=n1&&n4>=n3)
	med = n4;
if (n2>=n4&&n3>=n4&&n4>=n1&&n4>=n5)
	med = n4;
if (n2>=n4&&n3>=n4&&n4>=n1&&n4>=n5)
	med = n4;

if (n3>=n4&&n2>=n4&&n4>=n1&&n4>=n5)
	med = n4;
if (n3>=n4&&n2>=n4&&n4>=n5&&n4>=n1)
	med = n4;
if (n3>=n4&&n5>=n4&&n4>=n1&&n4>=n2)
	med = n4;
if (n3>=n4&&n5>=n4&&n4>=n2&&n4>=n1)
	med = n4;
if (n3>=n4&&n1>=n4&&n4>=n2&&n4>=n5)
	med = n4;
if (n3>=n4&&n1>=n4&&n4>=n2&&n4>=n5)
	med = n4;

if (n5>=n4&&n2>=n4&&n4>=n3&&n4>=n1)
	med = n4;
if (n5>=n4&&n2>=n4&&n4>=n1&&n4>=n3)
	med = n4;
if (n5>=n4&&n1>=n4&&n4>=n3&&n4>=n2)
	med = n4;
if (n5>=n4&&n1>=n4&&n4>=n2&&n4>=n3)
	med = n4;
if (n5>=n4&&n3>=n4&&n4>=n2&&n4>=n1)
	med = n4;
if (n5>=n4&&n3>=n4&&n4>=n2&&n4>=n1)
	med = n4;


if (n1>=n3&&n2>=n3&&n3>=n4&&n3>=n5)
	med = n3;
if (n1>=n3&&n2>=n3&&n3>=n5&&n3>=n4)
	med = n3;
if (n1>=n3&&n5>=n3&&n3>=n4&&n3>=n2)
	med = n3;
if (n1>=n3&&n5>=n3&&n3>=n2&&n3>=n4)
	med = n3;
if (n1>=n3&&n4>=n3&&n3>=n2&&n3>=n5)
	med = n3;
if (n1>=n3&&n4>=n3&&n3>=n2&&n3>=n5)
	med = n3;

if (n2>=n3&&n1>=n3&&n3>=n4&&n3>=n5)
	med = n3;
if (n2>=n3&&n1>=n3&&n3>=n5&&n3>=n4)
	med = n3;
if (n2>=n3&&n5>=n3&&n3>=n4&&n3>=n1)
	med = n3;
if (n2>=n3&&n5>=n3&&n3>=n1&&n3>=n4)
	med = n3;
if (n2>=n3&&n4>=n3&&n3>=n1&&n3>=n5)
	med = n3;
if (n2>=n3&&n4>=n3&&n3>=n1&&n3>=n5)
	med = n3;

if (n4>=n3&&n2>=n3&&n3>=n1&&n3>=n5)
	med = n3;
if (n4>=n3&&n2>=n3&&n3>=n5&&n3>=n1)
	med = n3;
if (n4>=n3&&n5>=n3&&n3>=n1&&n3>=n2)
	med = n3;
if (n4>=n3&&n5>=n3&&n3>=n2&&n3>=n1)
	med = n3;
if (n4>=n3&&n1>=n3&&n3>=n2&&n3>=n5)
	med = n3;
if (n4>=n3&&n1>=n3&&n3>=n2&&n3>=n5)
	med = n3;

if (n5>=n3&&n2>=n3&&n3>=n4&&n3>=n1)
	med = n3;
if (n5>=n3&&n2>=n3&&n3>=n1&&n3>=n4)
	med = n3;
if (n5>=n3&&n1>=n3&&n3>=n4&&n3>=n2)
	med = n3;
if (n5>=n3&&n1>=n3&&n3>=n2&&n3>=n4)
	med = n3;
if (n5>=n3&&n4>=n3&&n3>=n2&&n3>=n1)
	med = n3;
if (n5>=n3&&n4>=n3&&n3>=n2&&n3>=n1)
	med = n3;

if (n1>=n2&&n3>=n2&&n2>=n4&&n2>=n5)
	med = n2;
if (n1>=n2&&n3>=n2&&n2>=n5&&n2>=n4)
	med = n2;
if (n1>=n2&&n5>=n2&&n2>=n4&&n2>=n3)
	med = n2;
if (n1>=n2&&n5>=n2&&n2>=n3&&n2>=n4)
	med = n2;
if (n1>=n2&&n4>=n2&&n2>=n3&&n2>=n5)
	med = n2;
if (n1>=n2&&n4>=n2&&n2>=n3&&n2>=n5)
	med = n2;

if (n3>=n2&&n1>=n2&&n2>=n4&&n2>=n5)
	med = n2;
if (n3>=n2&&n1>=n2&&n2>=n5&&n2>=n4)
	med = n2;
if (n3>=n2&&n5>=n2&&n2>=n4&&n2>=n1)
	med = n2;
if (n3>=n2&&n5>=n2&&n2>=n1&&n2>=n4)
	med = n2;
if (n3>=n2&&n4>=n2&&n2>=n1&&n2>=n5)
	med = n2;
if (n3>=n2&&n4>=n2&&n2>=n1&&n2>=n5)
	med = n2;

if (n4>=n2&&n3>=n2&&n2>=n1&&n2>=n5)
	med = n2;
if (n4>=n2&&n3>=n2&&n2>=n5&&n2>=n1)
	med = n2;
if (n4>=n2&&n5>=n2&&n2>=n1&&n2>=n3)
	med = n2;
if (n4>=n2&&n5>=n2&&n2>=n3&&n2>=n1)
	med = n2;
if (n4>=n2&&n1>=n2&&n2>=n3&&n2>=n5)
	med = n2;
if (n4>=n2&&n1>=n2&&n2>=n3&&n2>=n5)
	med = n2;

if (n5>=n2&&n3>=n2&&n2>=n4&&n2>=n1)
	med = n2;
if (n5>=n2&&n3>=n2&&n2>=n1&&n2>=n4)
	med = n2;
if (n5>=n2&&n1>=n2&&n2>=n4&&n2>=n3)
	med = n2;
if (n5>=n2&&n1>=n2&&n2>=n3&&n2>=n4)
	med = n2;
if (n5>=n2&&n4>=n2&&n2>=n3&&n2>=n1)
	med = n2;
if (n5>=n2&&n4>=n2&&n2>=n3&&n2>=n1)
	med = n2;


System.out.println("Median: "+med);

//initialize mode
mode = 0;

//initialize the second value of mode
second = 0;

//if two numbers equal each other the first value will be the mode
if (n1==n2)
	mode = n1;
if (n1==n3)
	mode = n1;
if (n1==n4)
	mode = n1;
if (n1==n5)
	mode = n1;

if (n2==n3)
	mode = n2;
if (n2==n4)
	mode = n2;
if (n2==n5)
	mode = n2;

if (n3==n4)
	mode = n3;
if (n3==n5)
	mode = n3;

if (n4==n5)
	mode= n4;

//if two sets of numbers eqaul each other than there are two modes
if (n1==n2&n3==n4)
	mode = n1;
if (n1==n2&n3==n4)
	second = n3;
	
if (n1==n2&n4==n5)
	mode = n1;
if (n1==n2&n4==n5)
	second = n4;
	
if (n1==n2&n3==n5)
	mode = n1;
if (n1==n2&n3==n5)
	second = n3;
	
if (n2==n3&n4==n5)
	mode =n2;
if (n2==n3&n4==n5)
	second =n4;
	
if (n2==n3&n1==n4)
	mode =n2;
if (n2==n3&n1==n4)
	second =n1;
	
if (n2==n3&n1==n5)
	mode =n2;
if (n2==n3&n1==n5)
	second =n1;

if (n3==n4&n1==n5)
	mode = n3;
if (n3==n4&n1==n5)
	second = n1;
	
if (n3==n4&n2==n5)
	mode = n3;
if (n3==n4&n2==n5)
	second = n2;

if (n4==n5&n1==n3)
	mode = n4;
if (n4==n5&n1==n3)
	second = n1;
		
if (n1==n3&n2==n4)
	mode = n1;
if (n1==n3&n2==n4)
	second = n2;
	
if (n1==n3&n2==n5)
	mode = n1;
if (n1==n3&n2==n5)
	second = n2;

if (n1==n4&n2==n5)
	mode = n1;
if (n1==n4&n2==n5)
	second = n2;
	
if (n1==n4&n3==n5)
	mode = n1;
if (n1==n4&n3==n5)
	second = n3;

if (n1==n5&n2==n4)
	mode = n1;
if (n1==n5&n2==n4)
	second = n2;
	
if (n2==n4&n3==n5)
	mode = n2;
if (n2==n4&n3==n5)
	second = n3;
	

//if three numbers equal each other than that value equals the mode
if (n1==n2&&n1==n3)
	mode = n1;
if (n1==n2&&n1==n4)
	mode = n1;
if (n1==n2&&n1==n5)
	mode = n1;
if (n1==n3&&n1==n4)
	mode = n1;
if (n1==n4&&n1==n5)
	mode = n1;
if (n1==n3&&n1==n5)
	mode = n1;

if (n3==n4&&n3==n5)
	mode = n3;

if (n2==n4&&n2==n5)
	mode = n2;
if (n2==n3&&n2==n5)
	mode = n2;
if (n2==n3&&n2==n4)
	mode = n2;

//if four numbers equal each other than those values equal the mode
if (n1==n3&&n1==n4&&n1==n5)
	mode = n1;
if (n1==n2&&n1==n4&&n1==n5)
	mode = n1;
if (n1==n3&&n1==n2&&n1==n5)
	mode = n1;
if (n1==n3&&n1==n4&&n1==n2)
	mode = n1;

if (n2==n3&&n2==n4&&n2==n5)
	mode = n2;

//if all five numbers equal each other than all five numbers are the mode
if (n1==n2&&n1==n3&&n1==n4&&n1==n5)
	mode = n1;


System.out.println("Mode: "+mode +" " +second);




	
	





}
}